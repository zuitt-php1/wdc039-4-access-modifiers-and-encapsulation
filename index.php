<?php   require_once './code.php';?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WDC039-4 Access Modifiers and Encapsulation</title>
</head>
<body>
    
    <h2>Building</h2>
    <p>The name of the building is <?php echo $building->getName() ;?>.</p>
    <p>The <?php echo $building->getName() ;?> has <?php echo $building->getFloors() ;?> floors.</p>
    <p>The <?php echo $building->getName() ;?> is located at <?php echo $building->getAddress() ;?>.</p>
    <?php echo $building->setName("Caswynn Complex");?>
    <p>The name of the building has been changed to <?php echo $building->getName();?>.</p>

    <h2>Condominium Object Variable</h2>
    <p>The name of the condominium is <?php echo $condominium->getName() ;?>.</p>
    <p>The <?php echo $condominium->getName() ;?> has <?php echo $condominium->getFloors() ;?> floors.</p>
    <p>The <?php echo $condominium->getName() ;?> is located at <?php echo $condominium->getAddress() ;?>.</p>
    <?php echo $condominium->setName("Enzo Tower") ;?>
    <p>The name of the condominium has been changed to <?php echo $condominium->getName();?>.</p>


</body>
</html>