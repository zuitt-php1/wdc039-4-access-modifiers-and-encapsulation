<?php

class Building{
    
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName(){
        return $this->name;
    }


    public function getFloors(){
        return $this->floors;
    }

    public function getAddress(){
        return $this->address;
    }


    public function setName($name){ 
        $this->name = $name;
 
    }

    public function setFloors($floors){
        $this->floors = $floors;
    }

    public function setAddress($address){
        $this->address = $address;
    }

};

$building = new Building('Caswynn Building', 8, 'Timog Ave., Q.C., Philippines');

class Condominium extends Building{

    public function getName(){
        return $this->name;
    }


    public function getFloors(){
        return $this->floors;
    }

    public function getAddress(){
        return $this->address;
    }


    public function setName($name){ 
        $this->name = $name;
 
    }

    public function setFloors($floors){
        $this->floors = $floors;
    }

    public function setAddress($address){
        $this->address = $address;
    }

}

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines')


?>